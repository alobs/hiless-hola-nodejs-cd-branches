En esta configuración, utilizaremos dos ramas diferentes para desencadenar las implementaciones:

# Principal # 
Cualquier envío a la rama principal implementará el código en un entorno de prueba después de ejecutar las pruebas.

# Producción#
El código fusionado en la rama de producción se publicará automáticamente en el entorno de producción.

