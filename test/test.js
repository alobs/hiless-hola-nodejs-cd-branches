var request = require('supertest');
var app = require('../server.js')

describe('GET /', function() {
  it('displays "Hola mundo pro"', function(done) {
    // The line below is the core test of our app.
    request(app)
      .get('/')
      .expect(function(res) {
        if (res.text.indexOf('Hola mundo pro') == -1) throw new Error ("Missing Hello World!");
      })
      .expect(200, done);
  });
});

/*
var request = require('supertest');
var app = require('../server.js')

describe('GET /', function() {
	it('displays "Hola mundo"', function(done) {
		// The line below is the core test of our app
		request(app).get('/')
			.expect('Hola mundo', done);
	});
});
*/
